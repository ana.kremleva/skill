<?php
class Student extends User
{
    private $course;
    // Конструктор объекта:
    // Конструктор объекта:
    public function __construct($name, $age, $course)
    {
        parent::__construct($name, $age); // вызываем конструктор родителя
        $this->course = $course;
    }

    public function getCourse()
    {
        return $this->course;
    }
}
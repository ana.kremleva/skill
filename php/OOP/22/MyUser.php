<?php
class MyUser
{
    private $name;
    private $surname;
    private $bithday;
    private $age;

    public function __construct($name, $surname, $bithday)
    {
        $this->name = $name;
        $this->surname = $surname;
        if (preg_match('#^[0-9]{4}-[0-9]{2}-[0-9]{2}$#', $bithday)) {
            $this->bithday = $bithday;
        }
        $this->age = $this->calculateAge($bithday);




    }
    public function getName(){
        return $this->name;
    }

    public function getSurname(){
        return $this->surname;
    }

    public function getBithday(){
        return $this->bithday;
    }

    private function calculateAge($bithday){
        if (preg_match('#^([0-9]{4})-([0-9]{2})-([0-9]{2})$#', $bithday)){
            $today = getdate();
            preg_match('#^([0-9]{4})-([0-9]{2})-([0-9]{2})$#', $bithday, $bith);
            if ($today['mon'] < $bith[1]){
                $otvet = false;
            } elseif ($today['mon'] > $bith[2]){
                $otvet = true;
            } else {
                ($today['mday'] < $bith[3])? $otvet = false : $otvet = true ;
            }
            return ($otvet)? $today['year'] - $bith[1] + 1 : $today['year'] - $bith[1];
        }
    }

}
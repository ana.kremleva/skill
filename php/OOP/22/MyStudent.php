<?php
class MyStudent extends User{
    private $course;

    public function __construct($name, $age, $course)
    {
        $this->course = $course;
        parent::__construct($name, $age);
    }

    public function getCourse(){
        return $this->course;
    }
}
<?php
class User
{
    private $name;
    private $age;

    // Конструктор объекта:
    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    // Геттер для имени:
    public function getName()
    {
        return $this->name;
    }

    // Геттер для возраста:
    public function getAge()
    {
        return $this->age;
    }

    // Сеттер для возраста:
    public function setAge($age)
    {
        $this->age = $age;
    }
}

$user = new User('john', 25); // создаем объект с начальными данными

// Имя можно только читать, но нельзя поменять:
echo $user->getName(); // выведет 'john'
?><br><?
// Возраст можно и читать, и менять:
echo $user->getAge(); // выведет 25
?><br><?
echo $user->setAge(30); // установим возраст в значение 30
?><br><?
echo $user->getAge(); // выведет 30

class My
{
    private $name;
    private $surname;
    private $salary;

    public function __construct($name, $surname, $salary)
    {
        $this->surname = $surname;
        $this->name = $name;
        $this->salary = $salary;
    }

    public function getName(){
        return $this->name;
    }

    public function getSurname(){
        return $this->surname;
    }

    public function setSalary($salary){
        $this->salary = $salary;
    }

    public function getSalary(){
        return $this->salary;
    }
}


$user1 = new My('john', 25, 3000); // создаем объект с начальными данными

?><br><?
echo $user1->getName(); // выведет 'john'
?><br><?

echo $user1->getSurname();
?><br><?
echo $user1->getSalary();
?><br><?
echo $user1->setSalary(30);
?><br><?
echo $user1->getSalary();
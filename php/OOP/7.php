<?php

class User
{
    public $name;
    public $age;

    // Конструктор объекта:
    public function __construct($name, $age)
    {
        $this->name = $name; // запишем данные в свойство name
        $this->age = $age; // запишем данные в свойство age
    }
}

$user = new User('john', 25); // создадим объект, сразу заполнив его данными

echo $user->name; // выведет 'john'
echo $user->age; // выведет 25


class Employee
{
    public $name;
    public $age;
    public $salary;

    public function __construct($name, $age, $salary)
    {
        $this->age = $age;
        $this->salary = $salary;
        $this->name = $name;
    }

}

$user1 = new Employee('eric', 25, 1000);

$user2 = new Employee('kale',30,2000);
?><br><?

echo $user1->salary + $user2->salary;
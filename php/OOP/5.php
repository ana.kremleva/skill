<?php
class Users
{
    public $name;
    public $age;

    // Метод для изменения возраста юзера:
    public function setAge($age)
    {
        // Если возраст от 18 до 60:
        if ($age >= 18 and $age <= 60) {
            $this->age = $age;
        }
    }

    // Метод для добавления к возрасту:
    public function addAge($years)
    {

        $newAge = $this->age + $years;
        // Если НОВЫЙ возраст от 18 до 60:
        if ($newAge >= 18 and $newAge <= 60) {
            $this->age = $newAge; // обновим, если новый  возраст прошел проверку
			}
    }
}



class User
{
    public $name;
    public $age;

    // Метод для проверки возраста:
    public function isAgeCorrect($age)
    {
        if ($age >= 18 and $age <= 60) {
            return true;
        } else {
            return false;
        }
    }

    // Метод для изменения возраста юзера:
    public function setAge($age)
    {
        // Проверим возраст на корректность:
        if ($this->isAgeCorrect($age)) {
            $this->age = $age;
        }
    }

    // Метод для добавления к возрасту:
    public function addAge($years)
    {
        $newAge = $this->age + $years; // вычислим новый возраст

        // Проверим возраст на корректность:
        if ($this->isAgeCorrect($newAge)) {
            $this->age = $newAge; // обновим, если новый возраст прошел проверку
			}
    }
}

class MyUser
{
    public $name;
    public $age;

    public function ageCorrect($age){
        return ($age >=18 && $age <= 60);
    }

    public function setAge($age){
        (!$this->ageCorrect($age))? : $this->age = $age ;
    }


    public function addAge($age){
        $newAge = $this->age + $age;

        (!$this->ageCorrect($newAge))? : $this->age = $newAge;

    }

    public function subAge($age){
        $newAge = $this->age - $age;
        (!$this->ageCorrect($newAge))? : $this->age = $newAge;
    }
}


$user = new MyUser;

$user->age = 13;
echo $user->age . '<br>';

$user->setAge(18);
echo $user->age . '<br>';

$user->addAge(10);
echo $user->age . '<br>';


$user->subAge(3);
echo $user->age . '<br>';
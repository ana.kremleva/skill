<?php
class My
{
    private $num = [];

    public function __construct($num){
        $this->num = $num;
    }
    public function add($num){
        $this->num[]=$num;
    }

    public function getNum(){
        return $this->num;
    }

    public function getSum(){
        return array_sum($this->num);
    }

}


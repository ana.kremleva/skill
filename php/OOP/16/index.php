<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';

require_once 'Arr.php';
require_once 'My.php';


$arr = new Arr([1, 2, 3]); // создаем объект, записываем в него массив [1, 2, 3]
$arr->add(4); // добавляем в конец массива число 4
$arr->add(5); // добавляем в конец массива число 5

// Находим сумму элементов массива:
echo $arr->getSum(); // выведет 15


$arr1 = new Arr([1, 2, 3]);
echo $arr1->getSum();

echo (new Arr([1, 2, 3]))->getSum();

pre((new My([1,4,7,5,3,4,6]))->getSum());
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';

require_once 'User.php';
require_once 'City.php';
require_once 'Users.php';
require_once 'Prop.php';

$user = new User('john', 21);
echo $user->name; // выведет 'john'

$prop = 'name';
echo $user->$prop;

$user1 = new City('One', '29.06.1888', 2093);

$props = ['name', 'foundation', 'population'];

foreach ($props as $val){
    pre($user1->$val);
}

$props = ['surname', 'name', 'patronymic'];


$user2 = new Users('Иванов', 'Иван', 'Иванович');

$props = ['surname', 'name', 'patronymic'];
echo $user2->{$props[0]};
pre($user2->{$props[1]});
pre($user2->{$props[2]});

function getProp()
{
    return 'surname';
}

echo $user2->{getProp()};

$prop = new Prop('surname');


echo $user2->{$prop->value};

echo $user2->{$prop->getValue()};
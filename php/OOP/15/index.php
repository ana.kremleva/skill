<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';

require_once 'User.php';
//require_once 'City.php';
//require_once 'Users.php';
//require_once 'Prop.php';

$user = new User('john', 21);

$method = 'getName';
echo $user->$method();

$methods = ['getName', 'getAge'];
echo $user->{$methods[0]}();

$methods = ['method1' => 'getName', 'method2' => 'getAge'];
pre($user->{$methods['method1']}());
pre($user->{$methods['method2']}());
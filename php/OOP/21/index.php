<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';

require_once 'User.php';
require_once 'Student.php';

$student = new Student;

$student->setAge(24);    // укажем корректный возраст
echo $student->getAge(); // выведет 24 - возраст поменялся

$student->setAge(30);    // укажем некорректный возраст
echo $student->getAge(); // выведет 24 - возраст не поменялся
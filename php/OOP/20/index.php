<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';

require_once 'User.php';
require_once 'Me.php';
require_once 'Student.php';



$student = new Student();

$student->setAge(25);
$student->addOneYear();
$student->addOneYear();
$student->addOneYear();

pre($student);

$me = new Me;

$me->setAge(17);
$me->addOneYear();
$me->addOneYear();
$me->addOneYear();
$me->addOneYear();
pre($me);
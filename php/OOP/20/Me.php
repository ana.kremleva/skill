<?php
class Me extends User{
    private $course;

    public function setCourse($val){
        $this->course = $val;
    }

    public function getCourse(){
        return $this->course;
    }

    public function addOneYear(){
        $this->age ++;
    }

}
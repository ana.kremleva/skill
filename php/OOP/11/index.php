<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';
require_once 'User.php';
require_once 'City.php';


$users = [
    new User('john', 21),
    new User('eric', 22),
    new User('kyle', 23)
];
foreach ($users as $user) {
    echo $user->name . ' ' . $user->age . '<br>';
}
?><br><br><br><br><br><br><?

$city = [
    new City('one', 1000),
    new City('two',2000),
    new City('three', 3000),
    new City('four',4000),
    new City('six',6000)
];

foreach ($city as $val){
    echo $val->name . '  ' . $val->population . '<br>';
}


<?php
require_once 'User.php'; // подключаем наш класс
require_once 'My.php'; // подключаем наш класс

$user = new User('john', 25); // создаем объект с начальными данными

// Имя можно только читать, но нельзя поменять:
echo $user->getName(); // выведет 'john'
?><br><?
// Возраст можно и читать, и менять:
echo $user->getAge(); // выведет 25
?><br><?
echo $user->setAge(30); // установим возраст в значение 30
?><br><?
echo $user->getAge(); // выведет 30

?><br><??><br><??><br><?

$user1 = new My('john', 25, 3000); // создаем объект с начальными данными

?><br><?
echo $user1->getName(); // выведет 'john'
?><br><?

echo $user1->getSurname();
?><br><?
echo $user1->getSalary();
?><br><?
echo $user1->setSalary(30);
?><br><?
echo $user1->getSalary();
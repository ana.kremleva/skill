<?php
class User
{
    private $name;
    private $age;
}

$user = new User;

// Выдаст ошибку, так как свойство name - private:
//$user->name = 'john';

class MyUser
{
    public $name;
    public $age;


    public function setAge($age){
        (!$this->ageCorrect($age))? : $this->age = $age ;
    }


    public function addAge($age){
        $newAge = $this->age + $age;

        (!$this->ageCorrect($newAge))? : $this->age = $newAge;

    }

    public function subAge($age){
        $newAge = $this->age - $age;
        (!$this->ageCorrect($newAge))? : $this->age = $newAge;
    }

    private function ageCorrect($age){
        return ($age >=18 && $age <= 60);
    }
}

class Studenr
{
    public $name;
    public $cuourse;

    public function transferToNextCourse(){
        return  (!$this->isCourseCorrect())? $this->cuourse : $this->cuourse +1;

    }

    private function isCourseCorrect(){
        return ($this->cuourse >= 1 && $this->cuourse < 5);
    }
}

$r = new Studenr;

$r->cuourse = 5;

echo $r->transferToNextCourse();

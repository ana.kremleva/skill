<?php

class Driver extends Employee
{
    private $experience;
    private $category;

    public function setExperience($ex)
    {
        return $this->experience = $ex;
    }

    public function getExperience()
    {
        return $this->experience;
    }

    public function setCategory($cat)
    {
        $cats = ['A', 'B', 'C', 'D'];
        if (in_array($cat, $cats))
            return $this->category = $cat;
    }

    public function getCategory()
    {
        return $this->category;
    }

}
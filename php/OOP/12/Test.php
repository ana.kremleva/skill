<?php
class Test
{
    public $prop1;
    public $prop2;

    public function __construct()
    {
        $this->prop1 = 'value1'; // начальное значение свойства
        $this->prop2 = 'value2'; // начальное значение свойства
    }
}
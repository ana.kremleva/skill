<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';

require_once 'Test.php';
require_once 'Student.php';
require_once 'My.php';

$test = new Test;
echo $test->prop1; // выведет 'value1'
echo $test->prop2; // выведет 'value2'
?><br><br><br><br><?

$student = new Student('john'); // создаем объект класса

echo $student->getCourse(); // выведет 1 - начальное значение
?><br><?
$student->transferToNextCourse(); // переведем студента на следующий курс
echo $student->getCourse(); // выведет 2

$user = new My('yxy');

pre($user->getName());
pre($user->getCourse());
pre($user->transferToNextCourse());
pre($user->transferToNextCourse());
pre($user->transferToNextCourse());
pre($user->transferToNextCourse());
pre($user->transferToNextCourse());
pre($user->transferToNextCourse());
pre($user->transferToNextCourse());
pre($user->getCourse());


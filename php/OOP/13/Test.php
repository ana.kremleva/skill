<?php
class Test
{
    public $prop1;
    public $prop2;
    public $prop = 1 + 2; // найдем сумму чисел

    public function __construct()
    {
        $this->prop1 = 'value1'; // начальное значение свойства prop1
        $this->prop2 = 'value2'; // начальное значение свойства prop2
    }
}
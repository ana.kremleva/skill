<?php
class Student
{
    private $name;
    private $course = 1; // начальное значение курса

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function transferToNextCourse()
    {
        $this->course++;
    }
}
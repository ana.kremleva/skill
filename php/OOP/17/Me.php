<?php
class Me
{
    private $num = [];

    public function add ($num){
        $this->num[] = $num;
        return $this->num;
    }

    public function push ($num){
        return $this->num = array_merge($this->num, $num);
    }

    public function arSum(){
        return array_sum($this->num);
    }
}
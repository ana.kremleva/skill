<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/php/function.php';

require_once 'Arr.php';
require_once 'Me.php';


$arr = new Arr; // создаем объект

$arr->add(1); // добавляем в массив число 1
$arr->add(2); // добавляем в массив число 2
$arr->push([3, 4]); // добавляем группу чисел

echo $arr->getSum(); // находим сумму элементов массива

echo (new Arr)->add(1)->add(2)->push([3, 4])->getSum();

pre((new Arr)->add(3)->add(4)->push([3, 22])->getSum());
<?php
class User
{
    public $name;
    private $age; // объявим возраст приватным

    // Метод для чтения возраста юзера:
    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        if ($this->isAgeCorrect($age)) {
            $this->age = $age;
        }
    }

    private function isAgeCorrect($age)
    {
        return $age >= 18 and $age <= 60;
    }
}

$user = new User;

// Установим возраст:
$user->setAge(50);

// Прочитаем новый возраст:
echo $user->getAge();

class Employee
{
    private $name;
    private $age;
    private $salary;

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function setAge($age){
        (!$this->isAgeCorrect($age))? : $this->age = $age;

    }

    public function getAge(){
        return $this->age;
    }

    public function setSalary($salary){
        $this->salary = $salary;
    }

    public function getSalary(){
        return $this->salary . '$';
    }

    private function isAgeCorrect($age){
        return ($age >= 1 && $age <= 100);
    }
}


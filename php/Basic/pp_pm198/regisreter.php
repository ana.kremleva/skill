<meta charset="utf-8">
<?php
session_start();

include_once $_SERVER['DOCUMENT_ROOT'] . "/skill/php/help/function.php";
$host = 'localhost'; // имя хоста
$user = 'root';      // имя пользователя
$pass = '';          // пароль
$name = 'test';      // имя базы данных


$link = mysqli_connect($host, $user, $pass, $name);
mysqli_query($link, "SET NAMES 'utf8'");

(isset($_GET['i'])) ? $i = $_GET['i'] : $i = 5;

?>
<a href="<?= $_SERVER['PHP_SELF'] . '?i=' . $i ?>">Очисти POST данные перед проверкой задачи</a><br>

<?php


switch ($i) {
    case 5: ?>
        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=1"> Задача 1</a></p>
        <p>Реализуйте описанную выше регистрацию. После этого зарегистрируйте</p>
        <p>нового пользователя и авторизуйтесь под ним. Убедитесь, что все</p>
        <p> работает, как надо.</p>

        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=2"> Задача 2</a></p>
        <p>Модифицируйте ваш код так, чтобы кроме логина и пароля пользователю нужно было</p>
        <p>ввести еще и дату своего рождения и email. Сохраните эти данные в базу данных.</p>

        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=3"> Задача 3</a></p>
        <p>Модифицируйте ваш код так, чтобы в базу автоматически сохранялась</p>
        <p>дата регистрации.</p>

        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=4"> Задача 4</a></p>
        <p>Модифицируйте ваш код так, чтобы после регистрации пользователь</p>
        <p>автоматически становился авторизованным.</p>

        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=10"> Задача 5</a></p>
        <p>Запишите при регистрации в сессию еще и id пользователя.</p>


        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=6"> Задача 6</a></p>
        <p>Модифицируйте ваш код так, чтобы при отправке формы пароль</p>
        <p>сравнивался с его подтверждением. Если они совпадают - то</p>
        <p>продолжаем регистрацию, а если не совпадают - то выводим сообщение об этом.</p>

        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=7"> Задача 7</a></p>
        <p>Модифицируйте ваш код так, чтобы при попытке регистрации</p>
        <p>выполнялась проверка на занятость логина и, если он занят, -</p>
        <p>выводите сообщение об этом и просите ввести другой логин.</p>
        <?php break;
    case 1:
        /**
         *Реализуйте описанную выше регистрацию. После этого зарегистрируйте нового
         * пользователя и авторизуйтесь под ним. Убедитесь, что все работает,
         * как надо.
         */
        ?>
        <a href="login.php?i=1">Авторизация<br></a>
        <h3>Страница регистрации</h3>
        <form action="" method="POST">
            <input name="login" placeholder="Ваш логин">
            <input name="password" type="password" placeholder="Ваш пароль">
            <input type="submit">
        </form>

        <?php
        if (!empty($_POST['login']) and !empty($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            $query = "INSERT INTO useray (login, password) VALUES ('$login', '$password')";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));


        }
        ?>
        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=5">К задачам</a></p><?php
        break;
    case 2:
        /**
         *Модифицируйте ваш код так, чтобы кроме логина и пароля пользователю нужно
         * было ввести еще и дату своего рождения и email. Сохраните
         * эти данные в базу данных.
         */

        ?>
        <a href="login.php?i=2">Авторизация<br></a>
        <h3>Страница регистрации</h3>
        <form action="" method="POST">
            <input name="email" type="email" placeholder="Ваш email">
            <input name="rog" type="date" placeholder="Dатa вашего рождения">
            <input name="login" placeholder="Ваш логин">
            <input name="password" type="password" placeholder="Ваш пароль">

            <input type="submit">
        </form>

        <?php
        if (!empty($_POST['login']) and !empty($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            $rog = $_POST['rog'];


            $query = "SELECT * FROM useray LIMIT 1";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);

            (array_key_exists('data', $row)) ?: $query = "ALTER TABLE `useray` ADD `data` DATE NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            (array_key_exists('email', $row)) ?: $query = "ALTER TABLE `useray` ADD `email` VARCHAR(25) NOT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));

            $query = "INSERT INTO useray (login, password, email, data) VALUES ('$login', '$password', '$email', '$rog')";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));


        }

        ?>

        <p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=5">К задачам</a></p><?php


        break;
    case 3:
        /**
         *Модифицируйте ваш код так, чтобы в базу автоматически сохранялась дата регистрации.
         */
        ?>
        <a href="login.php?i=3">Авторизация<br></a>
        <h3>Страница регистрации</h3>
        <form action="" method="POST">
            <input name="email" type="email" placeholder="Ваш email">
            <input name="rog" type="date" placeholder="Dатa вашего рождения">
            <input name="login" placeholder="Ваш логин">
            <input name="password" type="password" placeholder="Ваш пароль">

            <input type="submit">
        </form>

        <?php
        if (!empty($_POST['login']) and !empty($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            $rog = $_POST['rog'];
            $datareg = date("Y-m-d H:i:s");

            $query = "SELECT * FROM useray LIMIT 1";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);

            (array_key_exists('data', $row)) ?: $query = "ALTER TABLE `useray` ADD `data` DATE NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            (array_key_exists('email', $row)) ?: $query = "ALTER TABLE `useray` ADD `email` VARCHAR(25) NOT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            (array_key_exists('dataregis', $row)) ?: $query = "ALTER TABLE `useray` ADD `dataregis` DATETIME NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));


            $query = "INSERT INTO useray (login, password, email, data, dataregis) VALUES ('$login', '$password', '$email', '$rog', '$datareg')";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));


        }


        ?><p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=5">К задачам</a></p><?php


        break;
    case 4:
        /**
         *Модифицируйте ваш код так, чтобы после регистрации пользователь
         * автоматически становился авторизованным.
         */
        ?>
        <a href="login.php?i=4">Авторизация<br></a>
        <a href="index.php?i=4">index</a>
        <a href="other.php?i=4">other</a>
        <h3>Страница регистрации</h3>
        <form action="" method="POST">
            <input name="email" type="email" placeholder="Ваш email">
            <input name="rog" type="date" placeholder="Dатa вашего рождения">
            <input name="login" placeholder="Ваш логин">
            <input name="password" type="password" placeholder="Ваш пароль">

            <input type="submit">
        </form>

        <?php
        if (!empty($_POST['login']) and !empty($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            $rog = $_POST['rog'];
            $datareg = date("Y-m-d H:i:s");

            $query = "SELECT * FROM useray LIMIT 1";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);

            (array_key_exists('data', $row)) ?: $query = "ALTER TABLE `useray` ADD `data` DATE NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            (array_key_exists('email', $row)) ?: $query = "ALTER TABLE `useray` ADD `email` VARCHAR(25) NOT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            (array_key_exists('dataregis', $row)) ?: $query = "ALTER TABLE `useray` ADD `dataregis` DATETIME NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));


            $query = "INSERT INTO useray (login, password, email, data, dataregis) VALUES ('$login', '$password', '$email', '$rog', '$datareg')";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));

            $_SESSION['auth'] = true; // пометка об авторизации


        }

        ?><p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=5">К задачам</a></p><?php


        break;
    case 10:
        /**
         *Запишите при регистрации в сессию еще и id пользователя.
         */
        ?>
        <a href="login.php?i=10">Авторизация<br></a>
        <a href="index.php?i=10">index</a>
        <a href="other.php?i=10">other</a>
        <h3>Страница регистрации</h3>
        <form action="" method="POST">
            <input name="email" type="email" placeholder="Ваш email">
            <input name="rog" type="date" placeholder="Dатa вашего рождения">
            <input name="login" placeholder="Ваш логин">
            <input name="password" type="password" placeholder="Ваш пароль">

            <input type="submit">
        </form>

        <?php
        if (!empty($_POST['login']) and !empty($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            $rog = $_POST['rog'];
            $datareg = date("Y-m-d H:i:s");

            $query = "SELECT * FROM useray LIMIT 1";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);

            (array_key_exists('data', $row)) ?: $query = "ALTER TABLE `useray` ADD `data` DATE NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            (array_key_exists('email', $row)) ?: $query = "ALTER TABLE `useray` ADD `email` VARCHAR(25) NOT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
            (array_key_exists('dataregis', $row)) ?: $query = "ALTER TABLE `useray` ADD `dataregis` DATETIME NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));


            $query = "INSERT INTO useray (login, password, email, data, dataregis) VALUES ('$login', '$password', '$email', '$rog', '$datareg')";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));

            $_SESSION['auth'] = true; // пометка об авторизации

            $id = mysqli_insert_id($link);
            $_SESSION['id'] = $id; // пишем id в сессию


        }

        ?><p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=5">К задачам</a></p><?php


        break;
    case 6:
        /**
         *Модифицируйте ваш код так, чтобы при отправке формы пароль
         * сравнивался с его подтверждением. Если они совпадают - то
         * продолжаем регистрацию, а если не совпадают - то выводим
         * сообщение об этом.
         */
        ?>
        <a href="login.php?i=6">Авторизация<br></a>
        <a href="index.php?i=6">index</a>
        <a href="other.php?i=6">other</a>
        <h3>Страница регистрации</h3>
        <form action="" method="POST">
            <input name="email" type="email" placeholder="Ваш email"><br><br>
            <input name="rog" type="date" placeholder="Dатa вашего рождения"><br><br>
            <input name="login" placeholder="Ваш логин"><br><br>
            <input name="password" type="password" placeholder="Ваш пароль"><br><br>
            <input type="password" name="confirm" placeholder="Потвердите пароль"><br><br>

            <input type="submit">
        </form>

        <?php
        if (!empty($_POST['login']) and !empty($_POST['password']) and !empty($_POST['confirm'])) {
            if ($_POST['password'] == $_POST['confirm']) {
                $login = $_POST['login'];
                $password = $_POST['password'];
                $email = $_POST['email'];
                $rog = $_POST['rog'];
                $datareg = date("Y-m-d H:i:s");

                $query = "SELECT * FROM useray LIMIT 1";
                $result = mysqli_query($link, $query) or die(mysqli_error($link));
                $row = mysqli_fetch_assoc($result);

                (array_key_exists('data', $row)) ?: $query = "ALTER TABLE `useray` ADD `data` DATE NULL DEFAULT NULL";
                $result = mysqli_query($link, $query) or die(mysqli_error($link));
                (array_key_exists('email', $row)) ?: $query = "ALTER TABLE `useray` ADD `email` VARCHAR(25) NOT NULL";
                $result = mysqli_query($link, $query) or die(mysqli_error($link));
                (array_key_exists('dataregis', $row)) ?: $query = "ALTER TABLE `useray` ADD `dataregis` DATETIME NULL DEFAULT NULL";
                $result = mysqli_query($link, $query) or die(mysqli_error($link));


                $query = "INSERT INTO useray (login, password, email, data, dataregis) VALUES ('$login', '$password', '$email', '$rog', '$datareg')";
                $result = mysqli_query($link, $query) or die(mysqli_error($link));

                $_SESSION['auth'] = true; // пометка об авторизации

                $id = mysqli_insert_id($link);
                $_SESSION['id'] = $id; // пишем id в сессию
                ?>
                <br>
                <a href="logout.php?i=6">ВЫЙТИ</a>
                <br>
                <?php
                echo 'Вы зарегестрированы и авторизованы';
            } else {
                echo 'Пароль не совпадаетс проверочным полем';
            }


        }

        ?><p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=5">К задачам</a></p><?php


        break;
    case 7:
        /**
         *Модифицируйте ваш код так, чтобы при попытке регистрации выполнялась
         * проверка на занятость логина и, если он занят, - выводите сообщение
         * об этом и просите ввести другой логин.
         */
        ?>
        <a href="login.php?i=7">Авторизация<br></a>
        <a href="index.php?i=7">index</a>
        <a href="other.php?i=7">other</a>
        <h3>Страница регистрации</h3>
        <form action="" method="POST">
            <input name="email" type="email" placeholder="Ваш email"><br><br>
            <input name="rog" type="date" placeholder="Dатa вашего рождения"><br><br>
            <input name="login" placeholder="Ваш логин"><br><br>
            <input name="password" type="password" placeholder="Ваш пароль"><br><br>
            <input type="password" name="confirm" placeholder="Потвердите пароль"><br><br>

            <input type="submit">
        </form>

        <?php
        if (!empty($_POST['login']) and !empty($_POST['password']) and !empty($_POST['confirm'])) {
            if ($_POST['password'] == $_POST['confirm']) {
                $login = $_POST['login'];
                $password = $_POST['password'];
                $email = $_POST['email'];
                $rog = $_POST['rog'];
                $datareg = date("Y-m-d H:i:s");


                // Проверяю если такой логин в бд
                $query = "SELECT * FROM `useray` WHERE login='$login'";
                $userLogin = mysqli_fetch_assoc(mysqli_query($link, $query));

                if (empty($userLogin)) {
                    $query = "SELECT * FROM useray LIMIT 1";
                    $result = mysqli_query($link, $query) or die(mysqli_error($link));
                    $row = mysqli_fetch_assoc($result);


                    // Добавляю новые поля в бд если их нет
                    (array_key_exists('data', $row)) ?: $query = "ALTER TABLE `useray` ADD `data` DATE NULL DEFAULT NULL";
                    $result = mysqli_query($link, $query) or die(mysqli_error($link));
                    (array_key_exists('email', $row)) ?: $query = "ALTER TABLE `useray` ADD `email` VARCHAR(25) NOT NULL";
                    $result = mysqli_query($link, $query) or die(mysqli_error($link));
                    (array_key_exists('dataregis', $row)) ?: $query = "ALTER TABLE `useray` ADD `dataregis` DATETIME NULL DEFAULT NULL";
                    $result = mysqli_query($link, $query) or die(mysqli_error($link));


                    /*$query = "SELECT * FROM usersya WHERE login='$login'";
                    $result = mysqli_query($link, $query) or die(mysqli_error($link));
                    $userLogin = mysqli_fetch_assoc($result);*/


                    $query = "INSERT INTO useray (login, password, email, data, dataregis) VALUES ('$login', '$password', '$email', '$rog', '$datareg')";
                    $result = mysqli_query($link, $query) or die(mysqli_error($link));

                    $_SESSION['auth'] = true; // пометка об авторизации

                    $id = mysqli_insert_id($link);
                    $_SESSION['id'] = $id; // пишем id в сессию
                    ?>
                    <br>
                    <a href="logout.php?i=7">ВЫЙТИ</a>
                    <br>
                    <?php
                    echo 'Вы зарегестрированы и авторизованы';
                } else {
                    echo 'Такой логин уже существует, вы не зарегистрированы';
                }

            } else {
                echo 'Пароль не совпадаетс проверочным полем';
            }


        }

        ?><p><a href="<?= $_SERVER['PHP_SELF'] ?>?i=5">К задачам</a></p><?php


        break;
}

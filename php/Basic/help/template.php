<?php
session_status();
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . "/skill/php/help/function.php";

    ?>
    <style>
        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 16px;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }

        .tabs {
            max-width: 350px;
            margin-left: auto;
            margin-right: auto;
        }

        .tabs>input[type="radio"] {
            display: none;
        }

        .tabs>input[type="radio"]:checked+label {
            background-color: #bdbdbd;
        }

        .tabs>div {
            /* скрыть контент по умолчанию */
            display: none;
            border: 1px solid #eee;
            padding: 10px 15px;
            border-radius: 4px;
        }

        /* отобразить контент, связанный с вабранной радиокнопкой (input type="radio") */
        #tab-btn-1:checked~#content-1,
        #tab-btn-2:checked~#content-2,
        #tab-btn-3:checked~#content-3,
        #tab-btn-4:checked~#content-4{
            display: block;
        }

        .tabs>label {
            display: inline-block;
            text-align: center;
            vertical-align: middle;
            user-select: none;
            background-color: #eee;
            border: 1px solid transparent;
            padding: 2px 8px;
            font-size: 16px;
            line-height: 1.5;
            border-radius: 4px;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out;
            margin-left: 6px;
            cursor: pointer;
            margin-bottom: 10px;
        }

        .tabs>label:first-of-type {
            margin-left: 0;
        }
    </style>
</head>
<body>
<?php
$host = 'localhost'; // имя хоста
$user = 'root';      // имя пользователя
$pass = '';          // пароль
$name = 'test';      // имя базы данных


$link = mysqli_connect($host, $user, $pass, $name);
mysqli_query($link, "SET NAMES 'utf8'");
?>
<a href="<?= $_SERVER['PHP_SELF']?>">clin</a>
<div class="tabs">
    <input type="radio" name="tab-btn" id="tab-btn-1" value="" checked>
    <label for="tab-btn-1">Вкладка 1</label>
    <input type="radio" name="tab-btn" id="tab-btn-2" value="">
    <label for="tab-btn-2">Вкладка 2</label>
    <input type="radio" name="tab-btn" id="tab-btn-3" value="">
    <label for="tab-btn-3">Вкладка 3</label>
    <input type="radio" name="tab-btn" id="tab-btn-4" value="">
    <label for="tab-btn-4">Вкладка 4</label>

    <div id="content-1">
        <?php
        /**
         * Реализуйте описанную выше авторизацию. Сделайте так, чтобы, если пользователь
         * прошел авторизацию - выводилось сообщение об этом, а если не прошел - то сообщение
         * о том, что введенный логин или пароль вбиты не правильно.
         */
        ?>
        <div style="color: blue">
            <p>Задача 1</p>
            <p>Реализуйте описанную выше авторизацию. Сделайте так, чтобы, если пользователь</p>
            <p>прошел авторизацию - выводилось сообщение об этом, а если не прошел - то сообщение</p>
            <p> о том, что введенный логин или пароль вбиты не правильно.</p>
        </div>
        <form action="" method="POST">
            <input name="login">
            <input name="password" type="password">
            <input type="submit">
        </form>
        <?php
        if (!empty($_POST['password']) and !empty($_POST['login'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            $query = "SELECT * FROM useray WHERE login='$login' AND password='$password'";
            $result = mysqli_query($link, $query);
            $users = mysqli_fetch_assoc($result);

            if (!empty($users)) {
                echo 'прошел авторизацию';
            } else {
                echo 'неверно ввел логин или пароль';
            }
        }
        ?>
    </div>
    <div id="content-2">
        Содержимое 2...
        <?php
        /**
         * Модифицируйте код так, чтобы в случае успешной авторизации форма для ввода пароля
         * и логина не показывалась на экране.
         */
        ?>
        <div style="color: blue">
            <p>Задача 2</p>
            <p>Модифицируйте код так, чтобы в случае успешной авторизации форма для ввода пароля</p>
            <p>и логина не показывалась на экране.</p>

        </div>

        <?php


        if (empty($_POST)):
            forma1: ?>
            <form action="" method="POST">
                <input name="login">
                <input name="password" type="password">
                <input type="submit">
            </form>
        <?php elseif (!empty($_POST['password']) and !empty($_POST['login'])):

            $login = $_POST['login'];
            $password = $_POST['password'];

            $query = "SELECT * FROM useray WHERE login='$login' AND password='$password'";
            $result = mysqli_query($link, $query);
            $users = mysqli_fetch_assoc($result);

            if (!empty($users)) {
                echo 'прошел авторизацию';
            } else {
                echo 'неверно ввел логин или пароль';
                goto forma1;
            }

        endif;

        ?>
    </div>
    <div id="content-3">
        Содержимое 3...
        <?php
        /**
         * Модифицируйте код так, чтобы в случае успешной авторизации происходил редирект на
         * страницу index.php.
         */
        ?>
        <div style="color: blue">
            <p>Задача 3</p>
            <p>Модифицируйте код так, чтобы в случае успешной авторизации происходил редирект на</p>
            <p>страницу index.php</p>

        </div>
        <?php


        if (empty($_POST)):
            forma2: ?>
            <form action="" method="POST">
                <input name="login">
                <input name="password" type="password">
                <input type="submit">
            </form>
        <?php elseif (!empty($_POST['password']) and !empty($_POST['login'])):

            $login = $_POST['login'];
            $password = $_POST['password'];

            $query = "SELECT * FROM useray WHERE login='$login' AND password='$password'";
            $result = mysqli_query($link, $query);
            $users = mysqli_fetch_assoc($result);

            if (!empty($users)) {
                header('Location: index.php');
            } else {
                echo 'неверно ввел логин или пароль';
                goto forma2;
            }

        endif;

        ?>
    </div>
    <div id="content-4">
        Содержимое 4...
        <?php
        /**
         * Модифицируйте код так, чтобы на странице index.php.php выводилось сообщение об
         * успешной авторизации. Решите задачу через флеш-сообщения на сессиях.
         */
        ?>
        <div style="color: blue">
            <p>Задача 4</p>
            <p>Модифицируйте код так, чтобы на странице index.php выводилось сообщение об</p>
            <p>успешной авторизации. Решите задачу через флеш-сообщения на сессиях.</p>

        </div>

        <?php
        if (empty($_POST)):
            forma3: ?>
            <form action="" method="POST">
                <input name="login">
                <input name="password" type="password">
                <input type="submit">
            </form>
        <?php elseif (!empty($_POST['password']) and !empty($_POST['login'])):

            $login = $_POST['login'];
            $password = $_POST['password'];

            $query = "SELECT * FROM useray WHERE login='$login' AND password='$password'";
            $result = mysqli_query($link, $query);
            $users = mysqli_fetch_assoc($result);

            if (!empty($users)) {
                $_SESSION['flash'] = 'прошел авторизацию';

                //header('Location: index.php');
            } else {
                echo 'неверно ввел логин или пароль';
                goto forma3;
            }

        endif;

        ?>
    </div>
</div>

</body>

</html>

<?php
/**
* Функция возвращает отсортированный массив
* @param массив
* @param ключ значения по которому сортируется массив
* @param вид сортировки
* @return отсортированный массив
*/
function arraySort(array $array, $key = 'sort', $sort = SORT_ASC): array
{
    switch ($sort) {
        case 'SORT_DESC':
            foreach ($array as $keyI => $valueI) {
                foreach ($array as $keyJ => $valueJ) {
                    if ($array[$keyI][$key] > $array[$keyJ][$key] ){
                    $q=$array[$keyI];
                    $array[$keyI] = $array[$keyJ];
                    $array[$keyJ] = $q;
                    }
                }
            };
            return $array;
            break;
        case 'SORT_ASC':
        default:
            foreach ($array as $keyI => $valueI) {
                foreach ($array as $keyJ => $valueJ) {
                    if ($array[$keyI][$key] < $array[$keyJ][$key] ){
                    $q=$array[$keyI];
                    $array[$keyI] = $array[$keyJ];
                    $array[$keyJ] = $q;
                    }
                }
            };
            return $array;
            break;
    }
    
};


/**
* Функция возвращает обрезаную строку
* @param строка
* @param длинна возвращаемой стоки
* @param значение, дописываемое в конце строки
* @return обрезаную строку и окончание строки
*/
function cutString($line, $length = 12 , $appends = '...'): string
{
    $line = mb_substr($line, 0, $length);
    return $line . $appends;
};


/**
* Функция возвращает отформатированный номер телефона
* @param строка
* @param формат форматирования, "DIGIT"- +71231231212, " "-+7 (123) 123-12-12
* @return отформатированный номер телефона
*/
function number ($nomer, $format = '') {
  preg_match_all('#\d#', $nomer, $matches1); 
  if ((count($matches1[0])) == 10)  $namber = '+7' . implode($matches1);
  if ((count($matches1[0])) == 11)  $numbers = '+7' . substr((implode($matches1[0])),1);
  preg_match_all('#(\+{1})(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})#', $numbers, $matches); 
  $numberSee = $matches[1][0] . $matches[2][0] . ' ' . '(' . $matches[3][0] . ') ' .  $matches[4][0] . '-' . $matches[5][0] . '-' . $matches[6][0] ;
  return ($format =='DIGIT')? $numbers: $numberSee;
};


/**
* Функция бирикс
* Функция возвращает значение заданного свойства
* @param id инфоблока
* @param id элемента
* @param код свойства
* @return значение заданного свойства
*/
function property ($iblock_id, $element_id, $name_property) {
  if (\Bitrix\Main\Loader::includeModule('iblock'))
  {
    $props = CIBlockElement::GetProperty(
      $iblock_id,  // id инфоблока
      $element_id, // id элемента
      array("sort" => "asc"), // массив для сортировки
      array()    // массив для фильтрации
    ); 
    while ($ob = $props->GetNext())
    {
      $VALUES[$ob['CODE']] = $ob['VALUE'];
    };
    return ($VALUES[$name_property]);
  }
};



/**
* Функция бирикс
* Функция возвращает массив данных элелемента
* @param id инфоблока
* @param id элемента, если не указывать выведи последний элемент инфоблока
* @return значение заданного свойства
*/
function propertyElement ($iblock_id, $element_id = '') {
  if (\Bitrix\Main\Loader::includeModule('iblock'))
  {
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>$iblock_id, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    while($ob = $res->GetNextElement()){ 
      $arFields = $ob->GetFields();  
      $arFields['PROPERTIES'] = $ob->GetProperties();
      if ($arFields['ID'] == $element_id) break;
    }
    return $arFields;
  }
};




/**
* Функция бирикс
* Функция возвращает массив данных преобразованный из объекта
* @param id инфоблока
* @param id элемента, если не указывать выведи последний элемент инфоблока
* @return значение заданного свойства
*/
function xmlAr($object, $arXml = []) {
    foreach ($object as $key => $value) {
      $arO = (array)$value;
      foreach ((array)$arO as $keyO => $valueO) {
        $arO[$keyO] = (is_object($valueO)? (array)$valueO : $valueO); 
      }
      $arXml[] = $arO;
    }
    return $arXml;
  };

/**
 * Функция проверяет если такая колонка и
 * добавляет новую колонку в БД с типом VARCHAR и длинной 15
 * @param $nameCol имя колонки
 * @param $link подключение к БД
 * @param $nameTeb имя таблицы
 * @param $tipCol тип данных
 * @param $sizeCol длина
 */


function tebleColumn($nameCol, $link, $nameTeb, $tipCol, $sizeCol = 15){
    $query ="SELECT * FROM $nameTeb LIMIT 1";
    $result = mysqli_query($link, $query) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    if (!array_key_exists($nameCol, $row)){
        if ($tipCol === "DATA"){

            //$query ="ALTER TABLE `$nameTeb` ADD $nameCol DATA NULL DEFAULT NULL";
            $query ="ALTER TABLE '$nameTeb' ADD `$nameCol` DATA NULL DEFAULT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
        } else {
            $query ="ALTER TABLE `$nameTeb` ADD `$nameCol` $tipCol($sizeCol) NOT NULL";
            $result = mysqli_query($link, $query) or die(mysqli_error($link));
        }


    }
}

/*
 * Полезные и простые функции для работы с PHP. Намеренно объявляются в глобальном namespace
 * */
if (!function_exists('pre')) {
    function pre($var, $die = false)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if ($die)
            die('Debug in PRE');
    }
}

if (!function_exists('vd')) {
    function vd($var, $die = false)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        if ($die)
            die('Debug in VD');
    }
}

if (!function_exists('writeEvent')) {
    function writeEvent($dump)
    {
        ulogging($dump, 'writeEvent', true);
    }
}

if (!function_exists('ulogging')) {
    /*
     * ВНИМАНИЕ! Перед использованием создать папку logs в upload и дать права на записать в папку
     * */
    function ulogging($input, $logname = 'debug', $dt = false)
    {
        $endLine = "\r\n"; #PHP_EOL не используется, т.к. иногда это нужно конфигурировать это

        $fp = fopen($_SERVER["DOCUMENT_ROOT"] . '/upload/logs/' . $logname . '.txt', "a+");

        if (is_string($input)) {
            $writeStr = $input;
        } else {
            $writeStr = print_r($input, true);
        }

        if ($dt) {
            fwrite($fp, date('d.m.Y H:i:s') . $endLine);
        }

        fwrite($fp, $writeStr . $endLine);

        fclose($fp);
        return true;
    }
}
$bitrixCMS = false;
if ($bitrixCMS == true) {
//получение цены без скидки |цена (int)$allProductPrices[0]["PRICE"]// value=3999.00;
    \Bitrix\Main\Loader::includeModule("catalog");
    $allProductPrices = \Bitrix\Catalog\PriceTable::getList(["filter" => ["=PRODUCT_ID" => $arResult['ID'],],])->fetchAll();
    $productPr = (int)$allProductPrices[0]["PRICE"];// цена целочисленная
    $productCode = $allProductPrices[0]["CURRENCY"];// код валюты
    $qqq = CPrice::GetBasePrice(7, false, false);


// получение скидки | скидка $arDiscounts[2]["VALUE"] // value=999;
// получает скидку только если скдка установленна на товар, торг\предл не получает
    $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
        $arParams['ELEMENT_ID'],
        $USER->GetUserGroupArray(),
        "N",
        SKLBOX_BASE_PRICE,
        SITE_ID
    );
    $productDs = $arDiscounts[2]["VALUE"];

//получение цены со скидкой
    (empty($arDiscounts)) ?: $productPrDs = $productPr - $productDs;

//получение общего числа товара из офферов
    foreach ($arResult["OFFERS"] as $value) {
        $colProduct += $value['PRODUCT']['QUANTITY'];
    }

}

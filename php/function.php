<?php
/*
 * Полезные и простые функции для работы с PHP. Намеренно объявляются в глобальном namespace
 * */
if (!function_exists('pre')) {
    function pre($var, $die = false)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if ($die)
            die('Debug in PRE');
    }
}

if (!function_exists('vd')) {
    function vd($var, $die = false)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        if ($die)
            die('Debug in VD');
    }
}
